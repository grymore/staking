import React from 'react';
import { Chip, Typography } from '@mui/material';
import { Done } from '@mui/icons-material';

export default function ConnectedChip() {
    return (
        <Chip
            title="Connected"
            icon={<Done />}
            label={
                <Typography
                    sx={{
                        color: '#FFFF'
                    }}
                    textAlign="center"
                    variant="p"
                    component="div"
                >
                    Connected
                </Typography>
            }
            variant="outlined"
        />
    );
}
