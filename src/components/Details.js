/* eslint-disable react/prop-types */
import React, { useCallback, useEffect, useRef, useState } from 'react';
import moment from 'moment';
import classes from '../styles/Staking.module.css';
import { Row, InputGroup, FormControl, Button } from 'react-bootstrap';
import Web3 from 'web3';
import { NFT_TOKEN } from '../constants/contracts';
import StakingAbi from '../abis/StakingAbi.json';
import nftAbi from '../abis/NFTToken.json';
import useSwal from '../hooks/useSwal';
import { calculateApy, toSmallUnit } from '../utils/string.utils';
import { APPROVE_AMOUNT } from '../constants/value';
import { DEFAULT_IMAGE } from '../constants/common';

const Details = (props) => {
    const { address, provider, phase } = props;
    const [inputValue, setInputValue] = useState('');
    const [loading, setLoading] = useState(false);
    const [stake, setStake] = useState(0);
    const [amount, setAmount] = useState(0);
    const [stakerTotal, setStakerTotal] = useState(0);
    const [rewardSymbol, setRewardSymbol] = useState(0);
    const [rewardDecimals, setRewardDecimals] = useState(0);
    const [day, setDay] = useState(0);
    const [ercSymbol, setErcSymbol] = useState('');
    const [erc20Contract, setErc20Contract] = useState('');
    const [erc20Balance, setErc20Balance] = useState(0);
    const [erc20Decimal, setErc20Decimal] = useState(0);
    const [button, setButton] = useState('Unlock');
    const [stakeDisabled, setStakeDisabled] = useState(false);
    const [claimDisabled, setClaimDisabled] = useState(true);
    const [endedTime, setEndedTime] = useState(0);
    const [endedTimeDisabled, setEndedTimeDisabled] = useState(true);
    const [yieldTotal, setYieldTotal] = useState(0);
    const [onStake, setOnStake] = useState(false);

    const { errror, info } = useSwal();

    const getTotalRewards = async () => {
        try {
            const _provider = new Web3(
                provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
            );
            const _stakeContract = new _provider.eth.Contract(StakingAbi, props.stakeContract);

            const _0 = await _stakeContract.methods.totalRewards(0).call();
            return [_0];
        } catch (err) {
            console.log(err.message, '@error getTotalRewards');
        }
    };

    const calculateYield = async (_decimals) => {
        try {
            const { toBN } = Web3.utils;
            const _provider = new Web3(
                provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
            );
            const _stakeContract = new _provider.eth.Contract(StakingAbi, props.stakeContract);
            const precision = _decimals;
            const dominator = toBN(10).pow(toBN(precision.toString()));
            const totalRewards = await getTotalRewards();
            const staked = await _stakeContract.methods.staker(address).call();
            const finalStaked = await _stakeContract.methods.finalStaked().call();
            if (staked.toString() === '0' || finalStaked.toString() === '0') return null;

            const profitRatio = toBN(staked).mul(dominator).div(toBN(finalStaked));
            const rewards = toBN(totalRewards[0]).mul(profitRatio);
            return {
                rewards: toSmallUnit(rewards.toString(), parseInt(props.baseErcDecimal)),
                staked: staked.toString(),
                ratio: profitRatio.toString()
            };
        } catch (err) {
            console.log(err, '@errYields');
            return null;
        }
    };

    const inputChangeHandler = (event) => {
        const _num = event.target.value;
        setAmount(_num);
    };

    const onClickUnstake = async () => {
        try {
            setOnStake(true);
            const _web3 = new Web3(
                provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
            );
            const erc20Contracts = new _web3.eth.Contract(nftAbi, erc20Contract);
            const stakingContract = new _web3.eth.Contract(StakingAbi, props.stakeContract);
            if (button === 'Unlock') {
                const approval = await erc20Contracts.methods.approve(props.stakeContract, APPROVE_AMOUNT).send({ from: address });
                if (approval) {
                    setOnStake(false);
                    setButton('Stake');
                }
            } else {
                if (phase !== 2) {
                    const stakerAmount = await stakingContract.methods.staker(address).call();
                    if (stakerAmount) {
                        const unstake = await stakingContract.methods.unstake(stakerAmount).send({ from: address });
                        if (unstake) {
                            setOnStake(false);
                            info('Success', 'You have been unstake ' + ercSymbol, 'success');
                            init();
                        }
                    }
                } else {
                    setOnStake(false);
                    info('Ooops', 'Staking can be used only in phase 1');
                }
            }
        } catch (err) {
            setOnStake(false);
            console.log(err, '@errorClickUnstaked');
            errror('Some error occured', err.message);
        }
    };

    const onClickClaim = async () => {
        try {
            const _web3 = new Web3(
                provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
            );
            const stakingContract = new _web3.eth.Contract(StakingAbi, props.stakeContract);
            const claim = await stakingContract.methods.claim().send({ from: address });
            if (claim) {
                init();
                info('Success', 'You have been claim ' + rewardSymbol, 'success');
            }
        } catch (err) {
            console.log(err, '@errOnClickClaim');
            errror('Some error occured', err.message);
        }
    };

    function toSmallUnitAsString(valueAsString, decimalAsString) {
        const { toBN } = Web3.utils;

        const BN = toBN;

        const toWei = Web3.utils.toWei;
        const decimalAdjustment = 18 - parseInt(decimalAsString);
        var bnValue = BN(toWei(valueAsString, 'ether'));
        bnValue =
            decimalAdjustment >= 0
                ? bnValue.div(BN('10').pow(BN(decimalAdjustment.toString())))
                : bnValue.mul(BN('10').pow(BN((decimalAdjustment * -1).toString())));
        return bnValue;
    }

    const onStakeClick = async () => {
        try {
            setOnStake(true);
            setButton('Loading ...');
            const _web3 = new Web3(
                provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
            );
            const erc20Contracts = new _web3.eth.Contract(nftAbi, erc20Contract);
            const stakingContract = new _web3.eth.Contract(StakingAbi, props.stakeContract);
            if (button === 'Unlock') {
                const accounts = await _web3.eth.getAccounts();
                const approval = await erc20Contracts.methods.approve(props.stakeContract, APPROVE_AMOUNT).send({ from: accounts[0] });
                if (approval) {
                    setOnStake(false);
                    setButton('Stake');
                }
            } else {
                const weiAmount = toSmallUnitAsString(amount.toString(), erc20Decimal.toString()).toString();
                if (phase === 1) {
                    const stake = await stakingContract.methods.stake(weiAmount).send({ from: address });
                    if (stake) {
                        setButton('stake');
                        setOnStake(false);
                        info('Success', 'You have been stake ' + ercSymbol, 'success');
                        init();
                    }
                } else {
                    setOnStake(false);
                    setButton('Stake');
                    info('Ooops', 'Staking can be used only in phase 1');
                }
            }
        } catch (err) {
            console.log(err);
            setOnStake(false);
            errror('Some error occured', err.message);
        }
    };

    const onClickMax = async () => {
        const unit = toSmallUnit(erc20Balance, erc20Decimal) * 0.99999;
        setAmount(unit);
    };

    const init = async () => {
        setLoading(true);
        setInputValue(0);
        try {
            if (window.ethereum) {
                const provider = new Web3(
                    Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
                );
                if (provider) {
                    setDay(props.day);
                    const stakeContract = new provider.eth.Contract(StakingAbi, props.stakeContract);
                    const nftContract = new provider.eth.Contract(nftAbi, NFT_TOKEN);
                    const nftBalance = await nftContract.methods.balanceOf(address).call();
                    const phase = await stakeContract.methods.phase().call();
                    const baseErcDecimal = props.baseErcDecimal;
                    // disable bentar
                    const stakeTotal = 0;
                    const totalStake = 0;
                    // const totalStake = await stakeContract.methods.totalStaked().call();
                    // const stakeTotal = await stakeContract.methods.staker(address).call();
                    const _quote = props.rewardContract;
                    const _baseERC = props.baseContract;
                    const decimals = props.precision;
                    const _preparationDate = await stakeContract.methods.endOfAgenda(0).call();
                    const _startDate = await stakeContract.methods.endOfAgenda(1).call();
                    const _stakeDate = await stakeContract.methods.endOfAgenda(2).call();
                    const _endedDate = await stakeContract.methods.endOfAgenda(3).call();

                    if (+phase <= 2) setClaimDisabled(true);
                    if (_endedDate !== 0 && _preparationDate !== 0 && _startDate !== 0 && _stakeDate !== 0) {
                        setEndedTime(_endedDate);

                        if (endedTime < Math.floor(new Date().getTime() / 1000)) {
                            setEndedTimeDisabled(true);
                        }
                    }

                    if (_baseERC) {
                        const ercContract = new provider.eth.Contract(nftAbi, _baseERC);
                        const _ercSymbol = props.ercSymbol;
                        const _ercBalance = await ercContract.methods.balanceOf(address).call();
                        const _allowance = await ercContract.methods.allowance(address, props.stakeContract).call();

                        if (+_allowance > +_ercBalance) {
                            setButton('Stake');
                        } else {
                            setButton('Unlock');
                        }
                        setErc20Balance(_ercBalance);
                        setErc20Decimal(baseErcDecimal);
                        setErcSymbol(_ercSymbol);
                        setErc20Contract(_baseERC);
                    }

                    if (_quote && props.totalRewards && props.quoteDecimals) {
                        const quoteContract = new provider.eth.Contract(nftAbi, _quote);
                        const _reward = await quoteContract.methods.symbol().call();
                        if (_reward) {
                            const _rewardTotal = toSmallUnit(+props.totalRewards, +props.quoteDecimals);
                            setRewardSymbol(_reward);
                        }
                    }

                    if (stakeTotal) {
                        const _staker = toSmallUnit(+stakeTotal, baseErcDecimal);
                        setStakerTotal(_staker);
                    }

                    if (totalStake && baseErcDecimal) {
                        const stakers = toSmallUnit(+totalStake, +baseErcDecimal);
                        setStake(stakers);
                    }

                    if (nftBalance && phase) {
                        const calculatedApy = calculateApy(
                            toSmallUnit(props.maxAmount, props.baseErcDecimal),
                            props.maxParticipant,
                            toSmallUnit(props.totalRewards, props.quoteDecimals),
                            365
                        );
                        if (+phase !== 1) {
                            setStakeDisabled(true);
                        }
                        if (+phase !== 1) {
                            setClaimDisabled(false);
                        }
                    }
                    setLoading(false);
                }
            } else {
                const provider = new Web3(
                    Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
                );
                if (provider) {
                    setDay(props.day);
                    const stakeContract = new provider.eth.Contract(StakingAbi, props.stakeContract);
                    const nftContract = new provider.eth.Contract(nftAbi, NFT_TOKEN);
                    const nftBalance = await nftContract.methods.balanceOf(address).call();
                    const phase = await stakeContract.methods.phase().call();
                    const baseErcDecimal = props.baseErcDecimal;

                    // disable bentar
                    const stakeTotal = 0;
                    const totalStake = 0;
                    // const totalStake = await stakeContract.methods.totalStaked().call();
                    // const stakeTotal = await stakeContract.methods.staker(address).call();

                    const _quote = props.rewardContract;
                    const _baseERC = props.baseContract;
                    const decimals = props.precision;
                    const _preparationDate = await stakeContract.methods.endOfAgenda(0).call();
                    const _startDate = await stakeContract.methods.endOfAgenda(1).call();
                    const _stakeDate = await stakeContract.methods.endOfAgenda(2).call();
                    const _endedDate = await stakeContract.methods.endOfAgenda(3).call();

                    if (decimals) setRewardDecimals(decimals);
                    const yielders = await calculateYield(decimals);
                    if (yielders !== null) {
                        setYieldTotal(yielders.rewards);
                    } else {
                        setYieldTotal(0);
                    }
                    if (+phase <= 2) setClaimDisabled(true);
                    if (_endedDate !== 0 && _preparationDate !== 0 && _startDate !== 0 && _stakeDate !== 0) {
                        setEndedTime(_endedDate);

                        if (endedTime < Math.floor(new Date().getTime() / 1000)) {
                            setEndedTimeDisabled(true);
                        }
                    }

                    if (_baseERC) {
                        const ercContract = new provider.eth.Contract(nftAbi, _baseERC);
                        const _ercSymbol = props.ercSymbol;
                        const _ercBalance = await ercContract.methods.balanceOf(address).call();
                        const _allowance = await ercContract.methods.allowance(address, props.stakeContract).call();

                        if (+_allowance > +_ercBalance) {
                            setButton('Stake');
                        } else {
                            setButton('Unlock');
                        }
                        setErc20Balance(_ercBalance);
                        setErc20Decimal(baseErcDecimal);
                        setErcSymbol(_ercSymbol);
                        setErc20Contract(_baseERC);
                    }

                    if (_quote && props.totalRewards && props.quoteDecimals) {
                        const quoteContract = new provider.eth.Contract(nftAbi, _quote);
                        const _reward = await quoteContract.methods.symbol().call();
                        if (_reward) {
                            const _rewardTotal = toSmallUnit(+props.totalRewards, +props.quoteDecimals);
                            setRewardSymbol(_reward);
                        }
                    }

                    if (stakeTotal) {
                        const _staker = toSmallUnit(+stakeTotal, baseErcDecimal);
                        setStakerTotal(_staker);
                    }

                    if (totalStake && baseErcDecimal) {
                        const stakers = toSmallUnit(+totalStake, +baseErcDecimal);
                        setStake(stakers);
                    }

                    if (nftBalance && phase) {
                        const calculatedApy = calculateApy(
                            toSmallUnit(props.maxAmount, props.baseErcDecimal),
                            props.maxParticipant,
                            toSmallUnit(props.totalRewards, props.quoteDecimals),
                            365
                        );
                        if (+phase !== 1) {
                            setStakeDisabled(true);
                        }
                        if (+phase !== 1) {
                            setClaimDisabled(false);
                        }
                    }
                    setLoading(false);
                }
            }
        } catch (err) {
            setLoading(false);
            console.log(err.message);
            if (err.message) {
                if (err.message.includes(`Returned values aren't valid`)) {
                    errror('Wrong Network', 'Please Switch your network to valid network');
                    return false;
                }
            }
            errror(err.message);
            return false;
        }
    };

    return (
        <>
            {phase === 1 ? (
                <>
                    <div className={classes.inputGroup}>
                        <InputGroup type="number" min="0" step="1" onChange={inputChangeHandler} value={inputValue}>
                            <img src={DEFAULT_IMAGE} alt="logo" style={{ width: '40px', padding: '8px' }} />
                            <FormControl
                                placeholder={amount}
                                value={loading ? 'Loading ...' : amount}
                                className={classes.inputField}
                                type="number"
                                onChange={(e) => {
                                    let _i = e.target.value;
                                    if (+_i > toSmallUnit(erc20Balance, erc20Decimal) * 0.99999) {
                                        setAmount(toSmallUnit(erc20Balance, erc20Decimal) * 0.99999);
                                    } else {
                                        setAmount(_i);
                                    }
                                }}
                                disabled={phase === 3}
                            />
                            <Button
                                className="btn-sm btn-primary rounded m-1 px-4"
                                id="button-addon2"
                                disabled={onStake}
                                onClick={onClickMax}
                            >
                                Max
                            </Button>
                        </InputGroup>
                    </div>
                </>
            ) : null}
            {phase === 1 || phase === 2 ? (
                <>
                    <b className={classes.Dot}>TVL: {stake}</b>
                    <b className={classes.Dot}>Stake: {stakerTotal}</b>
                    <b className={classes.Dot}>Balance: {toSmallUnit(erc20Balance, erc20Decimal)}</b>
                </>
            ) : null}
            {(phase === 3 && stakerTotal === 0 && phase !== 1) || (phase === 0 && endedTimeDisabled) ? (
                <Row>
                    <div className="col-12">
                        <Button
                            variant="secondary"
                            style={{
                                backgroundColor: 'grey'
                            }}
                            className="w-100"
                            onClick={onClickClaim}
                            disabled={claimDisabled}
                        >
                            {phase === 0 && endedTimeDisabled ? 'Upcoming' : 'Sold Out'}
                        </Button>
                    </div>
                </Row>
            ) : (
                <Row>
                    {button === 'Unlock' ? (
                        <div className="col-12">
                            <Button
                                variant="primary"
                                className="w-100"
                                onClick={() => {
                                    onStakeClick();
                                }}
                                disabled={onStake}
                            >
                                {button}
                            </Button>
                        </div>
                    ) : (
                        <>
                            <div className="col-6">
                                <Button
                                    variant="primary"
                                    className="w-100"
                                    onClick={() => {
                                        onStakeClick();
                                    }}
                                    disabled={onStake ? true : stakeDisabled}
                                >
                                    {button}
                                </Button>
                            </div>
                            <div className="col-6">
                                <Button variant="primary" className="w-100" onClick={onClickUnstake} disabled={phase === 2 ? true : false}>
                                    Unstake
                                </Button>
                            </div>
                        </>
                    )}
                    <>
                        <div className="pt-3">
                            <b className={classes.Dot}>
                                Yield:{' '}
                                {loading ? 'Loading ...' : `${toSmallUnit(yieldTotal / day, rewardDecimals).toFixed(2)} ${rewardSymbol}`} /
                                days
                            </b>
                        </div>
                        <div className="col-12 pt-3">
                            <Button variant="primary" className="w-100" onClick={onClickClaim} disabled={claimDisabled}>
                                Claim
                            </Button>
                        </div>
                    </>
                </Row>
            )}
            ;
        </>
    );
};

export default Details;
