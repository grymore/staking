/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useRef, useState } from 'react';
import moment from 'moment';
import classes from '../styles/Staking.module.css';
import { Row, Col, Card, Button } from 'react-bootstrap';
import useSwal from '../hooks/useSwal';
import { toCurrency, toSmallUnit } from '../utils/string.utils';
import DetailModal from './DetailModal';
import bnb from '../assets/bnbLogo.svg';
import Web3 from 'web3';
import StakingAbi from '../abis/StakingAbi.json';
import { Grid } from '@mui/material';
import ConnectWallet from 'lib/connectWallet';
import useWeb3 from 'hooks/useWeb3';
import walletConnectProvider from 'lib/connectWallet';

const calDur = (eventTime) => {
  if(+eventTime !== 0){
    let now = new Date().getTime();
    let timeleft = +eventTime - now;
    let days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
    let hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
  
    return {
      days,
      hours,
      minutes,
      seconds
    }
  }else{
    return {
      days: '00',
      hours: '00',
      minutes: '00',
      seconds: '00'
    }
  }
}

function Countdown({ eventTime, interval, phase }) {
    const [time, setTime] = useState({
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0
    })
    const timerRef = useRef(0);
    const timeChange = useCallback(() => {
      setTime(calDur(eventTime))
    }, [eventTime])

    const title = {
        0: 'Prepare Time',
        1: 'Start Time',
        2: 'Stake Time',
        3: 'Ended Time'
    };

    useEffect(() => {
        timerRef.current = setInterval(timeChange, interval);

        return () => {
            clearInterval(timerRef.current);
        };
    }, [eventTime]);


    return (
        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center'
            }}
        >
            <>
                <p className="presaleTitle">{title[phase]} in</p>
                <div
                    className="daysAndTime"
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row'
                    }}
                >
                    <div
                        className="HMS"
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'row'
                        }}
                    >
                        <div className="time">
                            <p
                                className="number"
                                style={{
                                    fontSize: '25px',
                                    margin: '0 .7rem'
                                }}
                            >
                                {time.days}
                            </p>
                            <span>Days</span>
                        </div>
                        <p
                            className="number"
                            style={{
                                fontSize: '20px'
                            }}
                        >
                            :
                        </p>
                        <div className="time">
                            <p
                                className="number"
                                style={{
                                    fontSize: '25px',
                                    margin: '0 .7rem'
                                }}
                            >
                                {time.hours}
                            </p>
                            <span>Hours</span>
                        </div>
                        <p
                            className="number"
                            style={{
                                fontSize: '20px'
                            }}
                        >
                            :
                        </p>
                        <div className="time">
                            <p
                                className="number"
                                style={{
                                    fontSize: '25px',
                                    margin: '0 .7rem'
                                }}
                            >
                                {time.minutes}
                            </p>
                            <span>Minutes</span>
                        </div>
                        <p
                            className="number"
                            style={{
                                fontSize: '20px'
                            }}
                        >
                            :
                        </p>
                        <div className="time">
                            <p
                                className="number"
                                style={{
                                    fontSize: '25px',
                                    margin: '0 .7rem'
                                }}
                            >
                                {time.seconds}
                            </p>
                            <span>Seconds</span>
                        </div>
                    </div>
                </div>
            </>
        </div>
    );
}


const Staking = (props) => {
    const [phase, setPhase] = useState(0);
    const [day, setDay] = useState(0);
    const [namePool, setNamePool] = useState('');
    const [endedTime, setEndedTime] = useState(0);
    const [endedTimeDisabled, setEndedTimeDisabled] = useState(true);
    const [preparationTime, setPreparationTime] = useState(0);
    const [startTime, setStartTime] = useState(0);
    const [stakeTime, setStakeTime] = useState(0);
    const [isConnected, setIsConnected] = useState(false);
    const [maxParticipant, setMaxParticipant] = useState('');
    const [openDetailModal, setOpenDetailModal] = useState(false);
    const [isUserStake, setIsUserStake] = useState(false);
    const [button, setButton] = useState({
        name: 'Detail',
        disabled: false
    });
    const { account, isConnect, provider } = walletConnectProvider;
    const { triggerConnect } = useWeb3();

    const modals = useSwal();

    const {
        errror,
        info
    } = modals;

    const unstake = async () => {
      try {
        setButton({
          disabled: true,
          name: 'Loading ...'
        });
        const _web3 = new Web3(
            provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
        );
        const stakingContract = new _web3.eth.Contract(StakingAbi, props.stakeContract);
        const stakerAmount = await stakingContract.methods.staker(account).call();
        if (stakerAmount) {
          const claim = await stakingContract.methods.claim().send({ from: account });
          if (claim) {
            const unstake = await stakingContract.methods.unstake(stakerAmount).send({ from: account });
            if (unstake) {
                info('Success', 'You have been unstake ' + props.ercSymbol, 'success');
                setButton({
                  disabled: false,
                  name: 'Unstake'
                });
            }
          }
        }
      } catch (err) {
          setButton({
            disabled: false,
            name: 'Unstake'
          });
          console.log(err, '@errorClickUnstaked');
          errror('Some error occured', err.message);
      }
    }

    const getStakerForUser = async (address = "") => {
      if(address === "" || address === null){
        return null;
      }else{
        const web3 = new Web3(
          provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
        );
        const stakingContract = new web3.eth.Contract(StakingAbi, props.stakeContract);
        const getStaker = await stakingContract.methods.staker(address).call();
        return getStaker;
      }
    }

    const initPhase = async () => {
      try {
        const web3 = new Web3(
          provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc' || 'https://bsc-dataseed.binance.org/' || 'https://bsc-dataseed1.defibit.io/')
        );
        const stakeContract = new web3.eth.Contract(StakingAbi, props.stakeContract);
        setNamePool(props.name);
        setDay(props.day);
        setMaxParticipant(props.maxParticipant);
        const _getPhases = await stakeContract.methods.phase().call();
        const isUserStake = await getStakerForUser(account);
        if(isUserStake !== null){
          if(+isUserStake !== 0){
            setIsUserStake(true);
            setButton({
              ...button
            });
          }else{
            if(+_getPhases !== 0 && +_getPhases !== 2){
              setIsUserStake(false);
              setButton({
                disabled: true,
                name: 'Not Stakers'
              });
            }
          }
        }
        if(+_getPhases !== 3){
          setPhase(_getPhases);
          const _preparationDate = await stakeContract.methods.endOfAgenda(0).call().then((data) => data + '000').catch(() => 0);
          const _startDate = await stakeContract.methods.endOfAgenda(1).call().then((data) => data + '000').catch(() => 0);
          const _stakeDate = await stakeContract.methods.endOfAgenda(2).call().then((data) => data + '000').catch(() => 0);
          const _endedDate = await stakeContract.methods.endOfAgenda(3).call().then((data) => data + '000').catch(() => 0);
          setEndedTime(+_endedDate);
          setPreparationTime(+_preparationDate);
          setStartTime(+_startDate);
          setStakeTime(+_stakeDate);
        }else{
          setPhase(3);
          setEndedTime(0);
          setPreparationTime(0);
          setStartTime(0);
          setStakeTime(0);
        }
      } catch (err) {
          // errror(err.message);
      }
    };

    const handleErrorStaking = (data) => {
        setOpenDetailModal(false);
        errror(data);
        setButton({
            name: 'Some Error Occured',
            disabled: true
        });
    };

    const onClickClaim = async () => {
      try {
        const _web3 = new Web3(
            provider || Web3.givenProvider || new Web3.providers.HttpProvider('https://rpc-bsc-main-01.3tokendigital.com/rpc')
        );
        const acc = await _web3.eth.getAccounts();
        console.log(acc, '@account2?')
        const stakingContract = new _web3.eth.Contract(StakingAbi, props.stakeContract);
        const claim = await stakingContract.methods.claim().send({ from: account.toLowerCase() });
        if (claim) {
            info('Success', `You've been claim!`, 'success');
        }
      } catch (err) {
          console.log(err, '@errOnClickClaim');
          errror('Some error occured', err.message);
      }
    }

    const check = async () => {
      initPhase();
      if (triggerConnect) setIsConnected(true);
      else setIsConnected(false);
    }

    React.useEffect(() => {
        check();
    }, [triggerConnect]);

    React.useEffect(() => {
      if(props.redirect !== ''){
        if(props.baseContract === props.redirect){
          setOpenDetailModal(true);
        }
      }
    }, [props.redirect]);

    React.useEffect(() => {
        if (phase === 0 && endedTimeDisabled) {
            setButton({
                disabled: false,
                name: 'Detail'
            });
        } else if (phase === 3 || (phase === 0 && endedTimeDisabled == false)) {
            setButton({
                disabled: false,
                name: 'Unstake'
            });
        } else {
            setButton({
                disabled: false,
                name: 'Detail'
            });
        }
    }, [phase, endedTimeDisabled]);

    return (
        <Col
            style={{
                position: 'relative'
            }}
            className="my-3"
        >
            <Card
                style={{
                    background: 'rgba(255, 255, 255, 0.2)',
                    backdropFilter: 'blur(40px)',
                    borderRadius: '24px'
                }}
            >
                {+phase === 0 ? (
                    <div
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: '-10px'
                        }}
                    >
                        <UpcommingSvg />
                    </div>
                ) : +phase === 3 ? (
                    <div
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: '-10px'
                        }}
                    >
                        <EndedSvg />
                    </div>
                ) : +phase === 1 ? (
                    <div
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: '-10px'
                        }}
                    >
                        <OpenSvg />
                    </div>
                ) : +phase === 2 ? (
                    <div
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: '-10px'
                        }}
                    >
                        <ProgressSvg />
                    </div>
                ) : null}
                <Card.Body>
                    <div className="text-center py-4">
                        <div
                            className="d-flex"
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                margin: '0 auto',
                                height: '100px'
                            }}
                        >
                            <img
                                src={props.baseToken ? props.baseToken : bnb}
                                alt="ttd"
                                style={{
                                    position: 'absolute',
                                    left: 0,
                                    marginLeft: '33%',
                                    top: '10%',
                                    width: '20%',
                                    zIndex: 10
                                }}
                            />
                            <img
                                src={props.rewardToken ? props.rewardToken : bnb}
                                alt="bnb"
                                style={{
                                    position: 'absolute',
                                    right: 0,
                                    marginRight: '33%',
                                    top: '10%',
                                    width: '20%'
                                }}
                            />
                        </div>
                        <Card.Title>{namePool}</Card.Title>
                        <h3 className="text-center">{day === 0 ? 1 : day} Day</h3>
                    </div>
                    <div className="card-text">
                        <div className="pb-3">
                            <b className={classes.Dot}>
                                APY <span>{props.apy + '%'} ++</span>
                            </b>
                            <p className={classes.Dot}>
                                Min stake{' '}
                                <span>{toCurrency(toSmallUnit(props.minAmount, props.baseErcDecimal || 18)) + ' ' + props.ercSymbol}</span>
                            </p>
                            <p className={classes.Dot}>
                                Max stake{' '}
                                <span>{toCurrency(toSmallUnit(props.maxAmount, props.baseErcDecimal || 18)) + ' ' + props.ercSymbol}</span>
                            </p>
                            <p className={classes.Dot}>
                                Max participant <span>{maxParticipant}</span>
                            </p>
                            <hr />
                            {+phase === 0 && endedTimeDisabled ? (
                                  <Countdown eventTime={preparationTime} interval={1000} phase={+phase} />
                            ) : null}
                            {+phase === 1 ? (
                                <>
                                    <Countdown eventTime={startTime} interval={1000} phase={+phase} />
                                </>
                            ) : null}
                            {+phase === 2 ? (
                                <>
                                    <Countdown eventTime={stakeTime} interval={1000} phase={+phase} />
                                </>
                            ) : null}
                            {+phase === 3 ? <Countdown eventTime={endedTime} interval={1000} phase={+phase} /> : null}
                        </div>
                        {!isConnected ? (
                            <Row>
                                <div className="col-12">
                                    <Button
                                        variant="secondary"
                                        style={{
                                            backgroundColor: 'grey'
                                        }}
                                        className="w-100"
                                        disabled={true}
                                    >
                                        Please connect to your account
                                    </Button>
                                </div>
                            </Row>
                        ) : (
                            <>
                                {
                                  button.name === 'Unstake' ? (
                                    <Grid container direction="row" alignItems={"center"} justifyContent="center" spacing={4}>
                                      <Grid item>
                                        <Button
                                          className="w-100"
                                          style={{
                                              background:
                                                  'linear-gradient(255.52deg, #0294AB -173.55%, #0294AB 71.9%, rgba(60, 89, 190, 0.7) 117.57%)',
                                              borderColor: 'transparent',
                                              width: '100%'
                                          }}
                                          onClick={onClickClaim}
                                        >
                                          Claim
                                        </Button>
                                      </Grid>
                                      <Grid item>
                                        <Button
                                          className="w-100"
                                          style={{
                                              background:
                                                  'linear-gradient(255.52deg, #0294AB -173.55%, #0294AB 71.9%, rgba(60, 89, 190, 0.7) 117.57%)',
                                              borderColor: 'transparent'
                                          }}
                                          onClick={() => button.name === 'Unstake' ? unstake() : setOpenDetailModal(true)}
                                          disabled={button.disabled}
                                        >
                                          {button.name}
                                        </Button>
                                      </Grid>
                                    </Grid>
                                  ) : (
                                    <div className={"col-12"}>
                                      <Button
                                          className="w-100"
                                          style={{
                                              background:
                                                  'linear-gradient(255.52deg, #0294AB -173.55%, #0294AB 71.9%, rgba(60, 89, 190, 0.7) 117.57%)',
                                              borderColor: 'transparent'
                                          }}
                                          onClick={() => setOpenDetailModal(true)}
                                          disabled={button.disabled}
                                      >
                                          {button.name}
                                      </Button>
                                    </div>
                                  )
                                }
                            </>
                        )}
                    </div>
                </Card.Body>
            </Card>
            <DetailModal
                open={openDetailModal}
                handleClose={() => setOpenDetailModal(false)}
                stakingEndDate={endedTime}
                baseErcDecimal={props.baseErcDecimal}
                baseContract={props.baseContract}
                day={props.day}
                maxStake={props.maxAmount}
                minStake={props.minAmount}
                apy={props.apy}
                pair={props.name}
                stakingStartDate={startTime}
                stakeContract={props.stakeContract}
                phase={phase}
                rewardContract={props.rewardContract}
                baseToken={props.baseToken}
                rewardToken={props.rewardToken}
                onErrorCallback={handleErrorStaking}
                stakeDate={stakeTime}
                preparationTime={preparationTime}
                provider={provider}
            />
        </Col>
    );
};

const EndedSvg = () => (
    <svg width="96" height="103" viewBox="0 0 96 103" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M57.6 0H96L0 96V57.841L57.6 0Z" fill="#878787" />
        <path
            d="M34.4346 47.2284L35.3208 48.1319L32.0833 51.3077L31.197 50.4042L34.4346 47.2284ZM26.4605 44.8727L32.4346 50.963L31.2843 52.0914L25.3102 46.001L26.4605 44.8727ZM31.3902 44.9699L32.256 45.8525L29.4409 48.6139L28.5751 47.7313L31.3902 44.9699ZM29.3342 42.0538L30.2245 42.9615L26.9995 46.125L26.1091 45.2173L29.3342 42.0538ZM35.468 36.0371L41.4421 42.1274L40.2876 43.2598L33.5543 41.5749L37.6984 45.7997L36.5481 46.928L30.574 40.8377L31.7243 39.7093L38.4701 41.3902L34.3219 37.1613L35.468 36.0371ZM44.8612 38.7735L43.5478 40.0619L42.6699 39.1502L43.975 37.87C44.3291 37.5226 44.5495 37.1532 44.6362 36.7618C44.7201 36.3676 44.6784 35.9625 44.5111 35.5466C44.3411 35.1278 44.0524 34.7107 43.6448 34.2952L43.337 33.9815C43.0225 33.6608 42.71 33.4119 42.3995 33.2349C42.0891 33.0579 41.7835 32.9528 41.4828 32.9195C41.1848 32.8835 40.8944 32.9193 40.6116 33.0271C40.3288 33.1349 40.0591 33.3146 39.8026 33.5662L38.439 34.9039L37.5486 33.9962L38.9122 32.6586C39.3194 32.2592 39.7573 31.9637 40.2259 31.7721C40.6974 31.5778 41.1816 31.4885 41.6787 31.5043C42.1758 31.5146 42.6636 31.6326 43.142 31.8582C43.6233 32.081 44.0786 32.4114 44.5081 32.8492L44.8076 33.1545C45.2344 33.5896 45.5559 34.0512 45.7721 34.5394C45.9885 35.0222 46.097 35.5121 46.0977 36.0093C46.1013 36.5038 45.9999 36.9862 45.7938 37.4566C45.5876 37.927 45.2767 38.3659 44.8612 38.7735ZM38.2179 33.3397L44.192 39.43L43.0417 40.5584L37.0675 34.468L38.2179 33.3397ZM52.2003 29.8016L53.0865 30.7052L49.8489 33.881L48.9627 32.9775L52.2003 29.8016ZM44.2262 27.446L50.2003 33.5363L49.05 34.6647L43.0759 28.5743L44.2262 27.446ZM49.1559 27.5432L50.0217 28.4258L47.2066 31.1872L46.3408 30.3046L49.1559 27.5432ZM47.0999 24.6271L47.9902 25.5348L44.7652 28.6983L43.8748 27.7906L47.0999 24.6271ZM56.1334 27.7165L54.8199 29.0048L53.942 28.0931L55.2471 26.8129C55.6013 26.4655 55.8216 26.0961 55.9083 25.7047C55.9922 25.3105 55.9505 24.9055 55.7833 24.4895C55.6133 24.0708 55.3245 23.6537 54.9169 23.2382L54.6092 22.9244C54.2946 22.6037 53.9821 22.3549 53.6717 22.1779C53.3612 22.0009 53.0556 21.8957 52.7549 21.8624C52.4569 21.8264 52.1665 21.8623 51.8837 21.9701C51.6009 22.0778 51.3313 22.2575 51.0747 22.5092L49.7111 23.8468L48.8207 22.9391L50.1843 21.6015C50.5915 21.2021 51.0294 20.9067 51.4981 20.715C51.9695 20.5207 52.4538 20.4315 52.9508 20.4473C53.4479 20.4576 53.9357 20.5756 54.4142 20.8011C54.8954 21.024 55.3508 21.3543 55.7802 21.7921L56.0798 22.0975C56.5065 22.5325 56.828 22.9942 57.0443 23.4824C57.2606 23.9651 57.3691 24.4551 57.3699 24.9523C57.3734 25.4468 57.2721 25.9292 57.0659 26.3996C56.8597 26.8699 56.5489 27.3089 56.1334 27.7165ZM49.49 22.2826L55.4641 28.373L54.3138 29.5013L48.3397 23.411L49.49 22.2826Z"
            fill="white"
        />
        <path d="M9 87V95.8542V102.5L0 95.8542L9 87Z" fill="#585B5F" />
    </svg>
);

const OpenSvg = () => (
    <svg width="96" height="103" viewBox="0 0 96 103" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M57.6 0H96L0 96V57.841L57.6 0Z" fill="#0BBA90" />
        <path
            d="M35.5607 41.6177L35.8684 41.9315C36.3197 42.3916 36.6646 42.8659 36.903 43.3543C37.1386 43.84 37.2693 44.3219 37.2951 44.8C37.3209 45.2781 37.2446 45.7387 37.0663 46.1817C36.888 46.6247 36.6079 47.0336 36.2258 47.4083C35.8494 47.7776 35.4366 48.0485 34.9874 48.2209C34.5411 48.3907 34.0777 48.4594 33.5974 48.4271C33.1171 48.3949 32.635 48.2577 32.1512 48.0154C31.6646 47.7704 31.1957 47.4179 30.7443 46.9578L30.4366 46.644C29.9825 46.1811 29.639 45.7055 29.4062 45.2171C29.1733 44.7287 29.044 44.2454 29.0182 43.7673C28.9897 43.2864 29.0646 42.8244 29.2429 42.3814C29.424 41.9357 29.7028 41.5282 30.0792 41.1589C30.4613 40.7841 30.8755 40.5119 31.3218 40.3422C31.7682 40.1725 32.2315 40.1065 32.7118 40.1443C33.1921 40.1765 33.6728 40.3151 34.1538 40.5601C34.6376 40.8023 35.1066 41.1548 35.5607 41.6177ZM34.7055 43.0721L34.3896 42.75C34.0641 42.4182 33.7476 42.1569 33.44 41.9661C33.1298 41.7725 32.8299 41.6481 32.5404 41.5928C32.2509 41.5375 31.9759 41.5528 31.7154 41.6387C31.4522 41.7218 31.2104 41.8714 30.9901 42.0875C30.767 42.3063 30.6128 42.5452 30.5274 42.804C30.4421 43.0573 30.4243 43.3292 30.474 43.6197C30.5237 43.9102 30.6409 44.2138 30.8257 44.5305C31.0105 44.8417 31.2657 45.1632 31.5912 45.495L31.9072 45.8171C32.2299 46.1462 32.5465 46.4075 32.8567 46.6011C33.1698 46.7919 33.4724 46.9164 33.7647 46.9744C34.057 47.027 34.3333 47.013 34.5938 46.9327C34.8542 46.8523 35.096 46.7027 35.319 46.4839C35.5421 46.2651 35.6964 46.0262 35.7817 45.7674C35.8671 45.5086 35.8835 45.2353 35.831 44.9475C35.7786 44.6542 35.6586 44.3506 35.4711 44.0366C35.2835 43.7227 35.0283 43.4012 34.7055 43.0721ZM40.2662 38.9226L38.6976 40.4613L37.8113 39.5578L39.3799 38.0191C39.6364 37.7675 39.8032 37.5218 39.8801 37.2823C39.957 37.0372 39.9606 36.8038 39.8909 36.5821C39.8185 36.3577 39.6879 36.1493 39.4992 35.9568C39.3159 35.77 39.1073 35.6327 38.8734 35.5448C38.6394 35.4569 38.3923 35.4421 38.1321 35.5003C37.8747 35.5559 37.6177 35.7095 37.3612 35.9611L36.169 37.1305L41.2528 42.3131L40.1025 43.4415L34.1284 37.3512L36.4708 35.0534C36.9449 34.5884 37.4326 34.2768 37.9341 34.1187C38.4357 33.955 38.9206 33.9362 39.389 34.0623C39.8574 34.1828 40.2776 34.4327 40.6497 34.8119C41.0354 35.2051 41.2785 35.6315 41.379 36.091C41.4823 36.5477 41.4405 37.0183 41.2535 37.5027C41.0694 37.9843 40.7402 38.4576 40.2662 38.9226ZM49.1774 32.7668L50.0637 33.6704L46.8261 36.8462L45.9398 35.9426L49.1774 32.7668ZM41.2033 30.4112L47.1775 36.5015L46.0272 37.6299L40.053 31.5395L41.2033 30.4112ZM46.1331 30.5084L46.9988 31.391L44.1837 34.1524L43.3179 33.2698L46.1331 30.5084ZM44.077 27.5923L44.9674 28.5L41.7424 31.6635L40.852 30.7558L44.077 27.5923ZM50.2108 21.5755L56.185 27.6659L55.0305 28.7983L48.2971 27.1134L52.4413 31.3382L51.2909 32.4665L45.3168 26.3762L46.4671 25.2478L53.213 26.9287L49.0647 22.6998L50.2108 21.5755Z"
            fill="white"
        />
        <path d="M9 87V95.8542V102.5L0 95.8542L9 87Z" fill="#10634F" />
    </svg>
);

const ProgressSvg = () => (
    <svg width="96" height="103" viewBox="0 0 96 103" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M57.6 0H96L0 96V57.841L57.6 0Z" fill="#0D489F" />
        <path d="M9 87V95.8542V102.5L0 95.8542L9 87Z" fill="#113770" />
        <path
            d="M18.2189 60.5492L16.6503 62.0879L15.764 61.1844L17.3326 59.6457C17.5891 59.3941 17.7559 59.1485 17.8327 58.9089C17.9097 58.6638 17.9133 58.4304 17.8436 58.2087C17.7712 57.9843 17.6406 57.7759 17.4519 57.5835C17.2686 57.3966 17.06 57.2593 16.8261 57.1714C16.5921 57.0835 16.345 57.0687 16.0848 57.127C15.8274 57.1825 15.5704 57.3361 15.3139 57.5877L14.1217 58.7571L19.2055 63.9398L18.0552 65.0681L12.0811 58.9778L14.4235 56.68C14.8976 56.215 15.3853 55.9034 15.8868 55.7453C16.3884 55.5816 16.8733 55.5628 17.3417 55.6889C17.8101 55.8094 18.2303 56.0593 18.6024 56.4385C18.988 56.8317 19.2312 57.2581 19.3317 57.7176C19.435 58.1744 19.3932 58.6449 19.2062 59.1293C19.022 59.6109 18.6929 60.0842 18.2189 60.5492ZM18.0057 53.1661L20.2185 50.9956C20.6814 50.5415 21.1472 50.2214 21.6158 50.0354C22.0844 49.8493 22.542 49.8053 22.9885 49.9036C23.4351 50.0018 23.8539 50.2503 24.2451 50.649C24.5542 50.9642 24.7665 51.2922 24.882 51.633C24.9975 51.9739 25.03 52.3223 24.9796 52.6781C24.932 53.0312 24.8154 53.3864 24.6296 53.7437L24.4504 54.2806L22.497 56.1968L21.5983 55.2973L23.0581 53.8653C23.2951 53.6328 23.4507 53.3981 23.5248 53.1613C23.5989 52.9245 23.6025 52.6938 23.5356 52.4694C23.4715 52.2423 23.3464 52.034 23.1604 51.8443C22.9607 51.6408 22.7494 51.5034 22.5263 51.4322C22.3005 51.3582 22.0685 51.3587 21.8302 51.4337C21.5919 51.5088 21.3515 51.6653 21.1089 51.9033L20.0464 52.9455L25.1302 58.1281L23.9799 59.2565L18.0057 53.1661ZM27.4182 55.8837L23.3334 54.5392L24.5465 53.341L28.5947 54.6147L28.6522 54.6733L27.4182 55.8837ZM31.6471 45.4566L31.9549 45.7703C32.4062 46.2304 32.7511 46.7047 32.9894 47.1932C33.2251 47.6788 33.3558 48.1607 33.3816 48.6388C33.4073 49.117 33.3311 49.5775 33.1528 50.0205C32.9745 50.4635 32.6943 50.8724 32.3123 51.2472C31.9358 51.6164 31.523 51.8873 31.0739 52.0598C30.6275 52.2295 30.1642 52.2982 29.6839 52.266C29.2035 52.2337 28.7215 52.0965 28.2376 51.8543C27.7511 51.6093 27.2821 51.2567 26.8308 50.7966L26.5231 50.4829C26.069 50.02 25.7255 49.5443 25.4927 49.0559C25.2598 48.5675 25.1305 48.0842 25.1047 47.6061C25.0762 47.1252 25.1511 46.6633 25.3294 46.2203C25.5105 45.7745 25.7892 45.367 26.1657 44.9977C26.5477 44.623 26.9619 44.3507 27.4083 44.181C27.8547 44.0113 28.318 43.9453 28.7982 43.9831C29.2786 44.0153 29.7592 44.1539 30.2403 44.3989C30.7241 44.6411 31.1931 44.9937 31.6471 45.4566ZM30.792 46.911L30.4761 46.5889C30.1506 46.257 29.834 45.9957 29.5265 45.8049C29.2162 45.6113 28.9163 45.4869 28.6268 45.4316C28.3373 45.3764 28.0623 45.3917 27.8019 45.4775C27.5386 45.5606 27.2969 45.7102 27.0766 45.9263C26.8535 46.1452 26.6993 46.384 26.6139 46.6428C26.5286 46.8961 26.5108 47.168 26.5605 47.4586C26.6102 47.7491 26.7274 48.0527 26.9122 48.3693C27.097 48.6805 27.3522 49.002 27.6777 49.3338L27.9936 49.6559C28.3164 49.985 28.6329 50.2463 28.9432 50.4399C29.2563 50.6307 29.5589 50.7552 29.8512 50.8133C30.1435 50.8658 30.4198 50.8519 30.6802 50.7715C30.9407 50.6912 31.1824 50.5416 31.4055 50.3227C31.6286 50.1039 31.7828 49.8651 31.8682 49.6062C31.9536 49.3474 31.97 49.0741 31.9175 48.7863C31.8651 48.493 31.7451 48.1894 31.5575 47.8755C31.37 47.5615 31.1148 47.24 30.792 46.911ZM37.8511 39.4695L40.1776 41.8412C40.2005 42.043 40.1814 42.3053 40.1203 42.6279C40.062 42.9477 39.9273 43.3069 39.7163 43.7054C39.5025 44.1011 39.1726 44.5178 38.7264 44.9555C38.3388 45.3357 37.9217 45.6189 37.4752 45.8052C37.0259 45.9887 36.5625 46.0685 36.0848 46.0446C35.6072 46.0207 35.1278 45.889 34.6468 45.6495C34.1657 45.4101 33.6967 45.0576 33.2399 44.5919L32.846 44.1903C32.3892 43.7246 32.0415 43.253 31.8031 42.7756C31.5674 42.2955 31.4366 41.8219 31.4107 41.3548C31.3876 40.885 31.4665 40.4328 31.6475 39.9981C31.8313 39.5606 32.1156 39.1532 32.5004 38.7757C33.0163 38.2696 33.5236 37.9389 34.0223 37.7835C34.5238 37.6254 35.0101 37.608 35.4813 37.7313C35.9497 37.8518 36.395 38.0826 36.8171 38.4237L35.696 39.5233C35.4575 39.3414 35.2142 39.2162 34.9663 39.1475C34.7156 39.076 34.4614 39.0791 34.2038 39.1567C33.9462 39.2288 33.6836 39.3962 33.4159 39.6588C33.19 39.8803 33.0344 40.1205 32.949 40.3794C32.8636 40.6382 32.8458 40.9101 32.8956 41.1951C32.9453 41.4801 33.0626 41.7782 33.2474 42.0893C33.4351 42.3977 33.6875 42.7137 34.0048 43.0372L34.4069 43.4471C34.727 43.7734 35.0449 44.0306 35.3608 44.2187C35.6739 44.404 35.978 44.5216 36.2731 44.5714C36.5682 44.6156 36.853 44.5908 37.1273 44.4967C37.4017 44.3972 37.6589 44.2298 37.8987 43.9945C38.1162 43.7811 38.2783 43.5866 38.3849 43.4108C38.4917 43.2295 38.5622 43.07 38.5967 42.9322C38.6312 42.7945 38.6475 42.6772 38.6457 42.5805L37.5255 41.4386L36.3167 42.6244L35.492 41.7836L37.8511 39.4695ZM36.4825 35.0419L38.6953 32.8714C39.1582 32.4173 39.624 32.0972 40.0926 31.9111C40.5612 31.725 41.0188 31.6811 41.4653 31.7793C41.9119 31.8775 42.3307 32.126 42.7219 32.5248C43.031 32.8399 43.2433 33.1679 43.3588 33.5088C43.4743 33.8497 43.5068 34.198 43.4564 34.5539C43.4088 34.907 43.2921 35.2622 43.1064 35.6195L42.9272 36.1564L40.9738 38.0726L40.075 37.1731L41.5349 35.7411C41.7719 35.5086 41.9275 35.2739 42.0016 35.0371C42.0757 34.8002 42.0793 34.5696 42.0124 34.3452C41.9483 34.1181 41.8232 33.9097 41.6372 33.7201C41.4375 33.5165 41.2262 33.3791 41.0031 33.3079C40.7773 33.2339 40.5453 33.2345 40.307 33.3095C40.0687 33.3846 39.8283 33.5411 39.5857 33.7791L38.5232 34.8212L43.607 40.0039L42.4566 41.1322L36.4825 35.0419ZM45.895 37.7595L41.8101 36.415L43.0233 35.2168L47.0715 36.4905L47.129 36.5491L45.895 37.7595ZM51.4814 30.5068L52.3677 31.4103L49.1301 34.5861L48.2438 33.6826L51.4814 30.5068ZM43.5073 28.1512L49.4814 34.2415L48.3311 35.3698L42.357 29.2795L43.5073 28.1512ZM48.437 28.2484L49.3028 29.131L46.4877 31.8923L45.6219 31.0097L48.437 28.2484ZM46.381 25.3323L47.2714 26.24L44.0463 29.4035L43.156 28.4958L46.381 25.3323ZM55.2313 25.4988C55.1164 25.3817 54.9957 25.2949 54.8692 25.2384C54.7428 25.1764 54.5966 25.1529 54.4307 25.1679C54.2649 25.1773 54.0641 25.2265 53.8285 25.3154C53.5929 25.3987 53.3072 25.5258 52.9713 25.6966C52.5993 25.8864 52.2359 26.0459 51.8811 26.175C51.5263 26.3042 51.183 26.3865 50.8512 26.422C50.5221 26.4547 50.2117 26.4255 49.9197 26.3343C49.6251 26.2403 49.3519 26.065 49.1003 25.8085C48.8541 25.5575 48.6828 25.2769 48.5863 24.9666C48.4926 24.6535 48.4723 24.326 48.5253 23.984C48.5756 23.6392 48.6949 23.2951 48.8833 22.9516C49.0745 22.6054 49.3332 22.2723 49.6595 21.9522C50.1112 21.5091 50.5823 21.2084 51.0728 21.0501C51.566 20.8892 52.0414 20.8606 52.4989 20.9644C52.9565 21.0683 53.3576 21.2959 53.7022 21.6473L52.5561 22.7715C52.3701 22.5819 52.1655 22.4542 51.9424 22.3886C51.7221 22.3201 51.49 22.3234 51.2462 22.3984C51.0052 22.4707 50.7634 22.6258 50.5208 22.8638C50.2866 23.0936 50.1256 23.3199 50.0378 23.5428C49.9528 23.763 49.9301 23.9713 49.9696 24.1678C50.0119 24.3616 50.1056 24.5323 50.2506 24.6801C50.36 24.7917 50.485 24.8661 50.6255 24.9033C50.7632 24.9378 50.9234 24.9421 51.106 24.9163C51.2858 24.8876 51.4866 24.8301 51.7084 24.7439C51.9303 24.6521 52.1773 24.5357 52.4493 24.3947C52.8824 24.1668 53.2846 23.9912 53.656 23.8677C54.0301 23.7415 54.3774 23.6744 54.6979 23.6664C55.0212 23.6557 55.3204 23.7069 55.5955 23.8201C55.8735 23.9305 56.1342 24.1098 56.3776 24.358C56.6347 24.6201 56.8129 24.9049 56.9122 25.2125C57.0114 25.5201 57.0359 25.8407 56.9858 26.1745C56.9356 26.5082 56.8122 26.8454 56.6155 27.186C56.4217 27.5239 56.1588 27.8556 55.827 28.1812C55.5314 28.4711 55.2003 28.7193 54.8337 28.9257C54.4671 29.1266 54.0888 29.2624 53.6986 29.3332C53.3057 29.4013 52.9192 29.381 52.5391 29.2723C52.1562 29.1609 51.802 28.9393 51.4765 28.6074L52.631 27.475C52.8197 27.6674 53.0118 27.8018 53.2072 27.8783C53.4026 27.9492 53.5999 27.9719 53.799 27.9462C53.9982 27.9149 54.195 27.8422 54.3895 27.7281C54.5839 27.614 54.7718 27.468 54.953 27.2902C55.1873 27.0604 55.3482 26.8396 55.4359 26.6278C55.5236 26.4104 55.5477 26.2062 55.5081 26.0152C55.4713 25.8215 55.379 25.6494 55.2313 25.4988ZM60.8883 19.9497C60.7734 19.8326 60.6527 19.7458 60.5262 19.6894C60.3997 19.6274 60.2536 19.6039 60.0877 19.6188C59.9219 19.6283 59.7211 19.6775 59.4855 19.7663C59.2499 19.8497 58.9641 19.9768 58.6283 20.1476C58.2563 20.3373 57.8929 20.4968 57.5381 20.626C57.1833 20.7552 56.8399 20.8375 56.5081 20.873C56.1791 20.9057 55.8686 20.8765 55.5767 20.7853C55.2821 20.6913 55.0089 20.516 54.7572 20.2594C54.5111 20.0085 54.3397 19.7278 54.2433 19.4175C54.1496 19.1045 54.1293 18.777 54.1823 18.435C54.2326 18.0902 54.3519 17.746 54.5403 17.4026C54.7315 17.0564 54.9902 16.7232 55.3165 16.4032C55.7682 15.96 56.2393 15.6594 56.7298 15.5011C57.223 15.3401 57.6984 15.3116 58.1559 15.4154C58.6134 15.5193 59.0145 15.7469 59.3592 16.0982L58.2131 17.2225C58.0271 17.0328 57.8225 16.9052 57.5994 16.8395C57.3791 16.7711 57.147 16.7744 56.9032 16.8494C56.6622 16.9216 56.4204 17.0768 56.1778 17.3147C55.9435 17.5445 55.7825 17.7709 55.6948 17.9937C55.6098 18.2139 55.587 18.4222 55.6266 18.6187C55.6689 18.8125 55.7626 18.9833 55.9075 19.1311C56.017 19.2426 56.1419 19.317 56.2824 19.3543C56.4202 19.3888 56.5804 19.3931 56.763 19.3672C56.9428 19.3386 57.1436 19.2811 57.3654 19.1949C57.5873 19.1031 57.8342 18.9867 58.1063 18.8457C58.5394 18.6178 58.9416 18.4421 59.313 18.3186C59.6871 18.1924 60.0344 18.1253 60.3549 18.1174C60.6782 18.1067 60.9774 18.1579 61.2525 18.271C61.5305 18.3814 61.7911 18.5607 62.0346 18.8089C62.2917 19.071 62.4699 19.3559 62.5691 19.6635C62.6684 19.971 62.6929 20.2917 62.6427 20.6254C62.5926 20.9592 62.4692 21.2964 62.2725 21.637C62.0787 21.9749 61.8158 22.3066 61.484 22.6321C61.1884 22.9221 60.8573 23.1702 60.4907 23.3766C60.1241 23.5775 59.7458 23.7134 59.3556 23.7842C58.9627 23.8522 58.5762 23.8319 58.196 23.7233C57.8132 23.6119 57.459 23.3903 57.1335 23.0584L58.2879 21.926C58.4767 22.1184 58.6687 22.2528 58.8641 22.3293C59.0596 22.4002 59.2569 22.4228 59.456 22.3971C59.6552 22.3659 59.852 22.2932 60.0464 22.1791C60.2409 22.0649 60.4288 21.9189 60.61 21.7411C60.8443 21.5114 61.0052 21.2906 61.0929 21.0787C61.1806 20.8613 61.2047 20.6572 61.1651 20.4662C61.1283 20.2725 61.036 20.1003 60.8883 19.9497ZM64.3419 18.6057C64.1778 18.4384 64.0968 18.2415 64.099 18.015C64.1012 17.7886 64.1999 17.5796 64.3951 17.3881C64.5903 17.1966 64.8011 17.102 65.0276 17.1041C65.2541 17.1063 65.4494 17.1911 65.6135 17.3584C65.7749 17.5229 65.8545 17.7184 65.8524 17.9449C65.8474 18.1686 65.7474 18.3762 65.5522 18.5677C65.357 18.7592 65.1475 18.8552 64.9237 18.8558C64.6973 18.8536 64.5033 18.7703 64.3419 18.6057ZM67.2047 15.7976C67.0406 15.6303 66.9596 15.4334 66.9618 15.2069C66.964 14.9804 67.0626 14.7714 67.2579 14.5799C67.4531 14.3884 67.6639 14.2938 67.8904 14.296C68.1169 14.2982 68.3122 14.3829 68.4763 14.5502C68.6377 14.7148 68.7173 14.9103 68.7151 15.1367C68.7102 15.3604 68.6102 15.568 68.415 15.7595C68.2198 15.951 68.0103 16.047 67.7865 16.0476C67.56 16.0455 67.3661 15.9621 67.2047 15.7976ZM70.0675 12.9894C69.9034 12.8221 69.8224 12.6252 69.8246 12.3987C69.8268 12.1722 69.9254 11.9632 70.1207 11.7718C70.3159 11.5803 70.5266 11.4856 70.7532 11.4878C70.9797 11.49 71.175 11.5747 71.3391 11.7421C71.5005 11.9066 71.5801 12.1021 71.5779 12.3286C71.573 12.5523 71.473 12.7599 71.2778 12.9513C71.0825 13.1428 70.8731 13.2389 70.6493 13.2395C70.4228 13.2373 70.2289 13.1539 70.0675 12.9894Z"
            fill="white"
        />
    </svg>
);

const UpcommingSvg = () => (
    <svg width="96" height="103" viewBox="0 0 96 103" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M57.6 0H96L0 96V57.841L57.6 0Z" fill="#FFC043" />
        <path d="M9 87V95.8542V102.5L0 95.8542L9 87Z" fill="#C19336" />
        <path
            d="M18.0301 66.7278C17.0732 67.6847 16.0601 68.1632 14.991 68.1632C13.9152 68.1566 12.8758 67.6517 11.8726 66.6486L7.96232 62.7383L9.24926 61.4514L13.1101 65.3122C14.4696 66.6717 15.7367 66.7641 16.9115 65.5893C18.0796 64.4212 17.9839 63.1574 16.6244 61.7978L12.7636 57.937L14.0307 56.6699L17.941 60.5802C18.9442 61.5834 19.449 62.6228 19.4556 63.6985C19.4622 64.7611 18.9871 65.7708 18.0301 66.7278ZM20.2284 53.64C20.7432 53.1253 21.3141 52.7788 21.941 52.6006C22.5746 52.4158 23.2115 52.4125 23.8516 52.5907C24.4918 52.7689 25.0792 53.1253 25.6137 53.6598C26.1483 54.1944 26.508 54.7851 26.6928 55.4318C26.871 56.072 26.8677 56.7089 26.6829 57.3424C26.5047 57.9694 26.1582 58.5403 25.6434 59.0551C24.9307 59.7678 24.1288 60.0945 23.2379 60.0351L25.8018 62.5991L24.5644 63.8365L17.3576 56.6297L18.5356 55.4516L19.2286 56.1446C19.1824 55.6892 19.2451 55.2504 19.4167 54.828C19.5948 54.399 19.8654 54.003 20.2284 53.64ZM24.4456 58.1344C24.901 57.679 25.1221 57.1544 25.1089 56.5604C25.1023 55.9598 24.8515 55.412 24.3565 54.9171C23.8615 54.4221 23.3138 54.1713 22.7132 54.1647C22.1192 54.1515 21.5945 54.3726 21.1392 54.828C20.8422 55.125 20.6442 55.4615 20.5452 55.8377C20.4396 56.2073 20.4429 56.5868 20.5551 56.9762C20.6673 57.3655 20.8884 57.7252 21.2184 58.0552C21.5483 58.3852 21.908 58.6063 22.2974 58.7185C22.6868 58.8307 23.0696 58.8373 23.4458 58.7383C23.8153 58.6327 24.1486 58.4314 24.4456 58.1344ZM31.9251 52.7734C31.3774 53.3211 30.7702 53.6973 30.1036 53.9019C29.4371 54.1065 28.7771 54.1263 28.1237 53.9613C27.4638 53.7897 26.8731 53.4432 26.3517 52.9219C25.8303 52.4005 25.4872 51.8131 25.3222 51.1598C25.1572 50.5064 25.1737 49.8497 25.3717 49.1898C25.5763 48.5232 25.9557 47.9127 26.5101 47.3583C27.0315 46.837 27.5925 46.4872 28.193 46.309C28.8002 46.1242 29.4008 46.1308 29.9947 46.3288L29.5988 47.8335C29.2028 47.7411 28.8299 47.751 28.4801 47.8632C28.1303 47.9622 27.8201 48.147 27.5496 48.4176C27.0876 48.8796 26.8566 49.4141 26.8566 50.0213C26.85 50.6219 27.0975 51.173 27.5991 51.6745C28.1006 52.1761 28.6517 52.4302 29.2523 52.4368C29.8528 52.4302 30.3874 52.1926 30.856 51.724C31.1266 51.4534 31.3147 51.1466 31.4203 50.8034C31.5259 50.447 31.5325 50.0708 31.4401 49.6748L32.9448 49.2788C33.1362 49.8794 33.1428 50.4866 32.9646 51.1004C32.7864 51.7009 32.4399 52.2586 31.9251 52.7734ZM37.5504 47.1481C37.0159 47.6826 36.4186 48.0489 35.7586 48.2469C35.0987 48.4449 34.442 48.4614 33.7886 48.2964C33.1353 48.1182 32.5479 47.7684 32.0265 47.2471C31.5052 46.7257 31.1587 46.1416 30.9871 45.4948C30.8221 44.8415 30.8386 44.1848 31.0366 43.5248C31.2346 42.8649 31.6008 42.2676 32.1354 41.733C32.6766 41.1919 33.2772 40.8223 33.9371 40.6243C34.5971 40.4263 35.2505 40.4131 35.8972 40.5847C36.5506 40.7497 37.138 41.0929 37.6593 41.6142C38.1807 42.1356 38.5272 42.7263 38.6988 43.3862C38.8704 44.033 38.8572 44.6864 38.6592 45.3464C38.4612 46.0063 38.0916 46.6069 37.5504 47.1481ZM36.4912 46.0888C36.9466 45.6334 37.171 45.1055 37.1644 44.5049C37.1578 43.9043 36.907 43.3566 36.412 42.8616C35.917 42.3666 35.3693 42.1158 34.7687 42.1092C34.1681 42.1026 33.6401 42.327 33.1848 42.7824C32.7294 43.2378 32.505 43.7657 32.5116 44.3663C32.5248 44.9603 32.7789 45.5047 33.2739 45.9997C33.7688 46.4947 34.3133 46.7488 34.9073 46.762C35.5078 46.7686 36.0358 46.5442 36.4912 46.0888ZM42.8123 31.0561C43.4789 30.3896 44.2016 30.0563 44.9803 30.0563C45.7525 30.0497 46.5246 30.4325 47.2968 31.2046L50.3261 34.2339L49.0886 35.4713L46.2178 32.6004C45.7558 32.1385 45.307 31.8943 44.8714 31.8679C44.4293 31.8349 44.0168 32.0098 43.634 32.3926C43.2182 32.8083 43.0235 33.2802 43.0499 33.8082C43.0697 34.3296 43.3337 34.8443 43.8419 35.3525L46.5246 38.0353L45.2872 39.2727L42.4164 36.4018C41.9544 35.9399 41.5056 35.6957 41.07 35.6693C40.6278 35.6363 40.2154 35.8112 39.8326 36.194C39.4102 36.6163 39.2089 37.0882 39.2287 37.6096C39.2551 38.1244 39.5257 38.6391 40.0405 39.1539L42.7232 41.8367L41.4858 43.0741L36.1995 37.7878L37.3775 36.6097L38.0507 37.2829C38.0111 36.8473 38.0771 36.4183 38.2487 35.996C38.4203 35.5736 38.6875 35.1809 39.0505 34.8179C39.4465 34.4219 39.8722 34.1481 40.3276 33.9963C40.7829 33.8313 41.2449 33.8049 41.7135 33.9171C41.6541 33.4221 41.7201 32.9271 41.9115 32.4321C42.0963 31.9306 42.3966 31.4719 42.8123 31.0561ZM46.621 27.3662L47.8584 26.1288L53.1448 31.4151L51.9073 32.6526L46.621 27.3662ZM46.3735 25.8714C46.1491 26.0958 45.8917 26.2146 45.6014 26.2278C45.3044 26.2344 45.0503 26.1321 44.8391 25.9209C44.6279 25.7097 44.5289 25.4589 44.5421 25.1685C44.5487 24.8716 44.6642 24.6109 44.8886 24.3865C45.113 24.1621 45.3704 24.0433 45.6608 24.0301C45.9445 24.0103 46.1887 24.1027 46.3933 24.3073C46.6111 24.5251 46.72 24.7858 46.72 25.0894C46.72 25.3797 46.6045 25.6404 46.3735 25.8714ZM52.4227 21.4457C53.0893 20.7792 53.8186 20.4393 54.6105 20.4261C55.4025 20.4129 56.1812 20.7891 56.9468 21.5546L59.976 24.5839L58.7386 25.8213L55.8678 22.9504C55.4058 22.4885 54.9504 22.2509 54.5016 22.2377C54.0463 22.2179 53.6173 22.4093 53.2147 22.8119C52.7593 23.2672 52.5382 23.7655 52.5514 24.3067C52.558 24.8412 52.8187 25.3659 53.3335 25.8807L56.0064 28.5536L54.7689 29.791L49.4826 24.5047L50.6606 23.3266L51.3437 24.0097C51.3041 23.5609 51.3767 23.1187 51.5615 22.6832C51.7463 22.2476 52.0334 21.8351 52.4227 21.4457ZM61.5546 12.4327L66.039 16.9172C67.9001 18.7783 67.8803 20.6592 65.9796 22.5599C65.4715 23.0681 64.9237 23.4838 64.3363 23.8072C63.7555 24.1372 63.1946 24.3286 62.6534 24.3814L62.2772 22.8965C62.6996 22.8569 63.1418 22.7183 63.6037 22.4807C64.0789 22.2431 64.498 21.9428 64.861 21.5798C65.4418 20.9991 65.7354 20.4414 65.742 19.9068C65.7486 19.3722 65.4814 18.8344 64.9402 18.2932L64.663 18.016C64.6828 18.4582 64.6003 18.8905 64.4155 19.3128C64.2307 19.7352 63.9568 20.1279 63.5939 20.4909C63.0923 20.9925 62.5313 21.3422 61.9109 21.5402C61.2906 21.725 60.6669 21.7415 60.0399 21.5897C59.4196 21.4313 58.8586 21.1014 58.357 20.5998C57.8554 20.0982 57.5288 19.5405 57.377 18.9268C57.2252 18.2998 57.245 17.6794 57.4364 17.0657C57.6344 16.4453 57.9841 15.8843 58.4857 15.3827C58.8685 15 59.2777 14.7162 59.7132 14.5314C60.1554 14.34 60.6108 14.2674 61.0794 14.3136L60.3765 13.6107L61.5546 12.4327ZM62.7326 19.2534C63.2012 18.7849 63.4454 18.2635 63.4652 17.6893C63.485 17.102 63.2705 16.5839 62.8217 16.1351C62.3795 15.6929 61.868 15.485 61.2873 15.5114C60.7131 15.5312 60.1917 15.7754 59.7231 16.244C59.248 16.7192 58.9972 17.2471 58.9708 17.8279C58.951 18.4021 59.1622 18.9103 59.6044 19.3524C60.0531 19.8012 60.5679 20.019 61.1487 20.0058C61.7294 19.9794 62.2574 19.7286 62.7326 19.2534Z"
            fill="white"
        />
    </svg>
);

export default Staking;
