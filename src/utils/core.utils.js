export function createData(name, totalRewards, apy, maxParticipant, status) {
    return { name, totalRewards, apy, maxParticipant, status };
}

export const makeRandomDeg = () => {
    return Math.floor(Math.random() * 1000000);
};
