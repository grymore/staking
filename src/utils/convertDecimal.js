export const convertDecimal = (amount, decimal) => {
    return amount / Math.pow(10, decimal);
};