import { WalletConnectConnector } from '@web3-react/walletconnect-connector';
import { WCCHAIN } from '../utils/web3.chains';

export const walletconnect = new WalletConnectConnector({
    qrcode: true,
    rpc: WCCHAIN,
    bridge: 'https://bridge.walletconnect.org'
});
