import { WalletLinkConnector } from '@web3-react/walletlink-connector';

export const walletlink = new WalletLinkConnector({
    supportedChainIds: [56, 97],
    appName: 'staking',
    darkMode: true
});
